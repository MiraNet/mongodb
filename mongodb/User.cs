﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace mongodb
{
    class User
    {
        private static IMongoDatabase db = new MongoClient("mongodb://localhost").GetDatabase("learning");

        [BsonId]
        public ObjectId Id { get; set; }

        public string username { get; set; }
        public string name { get; set; }
        public Communication Communication { get; set; }

        public static async Task<List<User>> All()
        {
            List<User> data = await db.GetCollection<User>("users").AsQueryable<User>().ToListAsync();
            return data;
        }

        public static User FindUsername(string username)
        {
            User user = db.GetCollection<User>("users").AsQueryable<User>().SingleOrDefault(p => p.username == username);
            return user;
        }

        public async Task Save()
        {
            var collection = db.GetCollection<User>("users");
            await collection.InsertOneAsync(this);
        }
    }

    class Communication
    {
        public string email { get; set; }
        public string phone { get; set; }
    }
}
