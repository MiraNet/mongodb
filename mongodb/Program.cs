﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace mongodb
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Get all users documents
                List<User> users = User.All().GetAwaiter().GetResult();
                Console.WriteLine(users.ToJson());

                //Get first user document by username field
                //Console.WriteLine("Введите никнейм пользователя для поиска: ");
                //User user = User.FindUsername(Console.ReadLine());
                //Console.WriteLine(user.ToJson());

                //Write and creating new user document
                //WriteNewData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        private static void WriteNewData()
        {
            Console.WriteLine("Создание пользователя");
            Console.WriteLine("Введите имя");
            string name = Console.ReadLine();
            Console.WriteLine("Введите никнейм");
            string username = Console.ReadLine();
            Console.WriteLine("Введите email");
            string email = Console.ReadLine();      
            Console.WriteLine("Введите телефон");
            string phone = Console.ReadLine();

            User user = new User
            {
                username = username,
                name = name,
                Communication = new Communication
                {
                    email = email,
                    phone = phone
                }
            };
            user.Save().GetAwaiter().GetResult();
        } 

    }
}
